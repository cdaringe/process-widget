# process-widget

small utility to run a process with different exit behaviors.

you can run this executable in a variety of different modes to simulate:

- long running processes
- short running, successful processes
- short running, failing process

## install

`npm install [-g] process-widget`

## usage

`process-widget --help`

```bash
Usage: process-widget [options]

Options:

  -h, --help         output usage information
  -V, --version      output the version number
  run                run the widget indefinitely [default]
  run-graceful-exit  run and exit gracefully after 1 second
  run-crash          ran and exit with error code 1 between 0.5-1.5 seconds
```

## examples

- start a long running process

```
$ ~/node/process-widget$ process-widget # or process-widget run
Placeat enim culpa quo ut.
Esse asperiores inventore quas quidem aperiam eligendi aut omnis quo.
... [outputs 1 random sentence/second to stdout]
```

- start a short running, successful process

```
$ ~/node/process-widget$ process-widget run-graceful-exit && echo "success"
Voluptatibus repellat iste consequatur corrupti architecto quia delectus voluptas.
Laborum et dicta in minima cum aut et est.
success
```

- start a short running, crashing process

```
$ ~/node/process-widget$ process-widget run-crash && echo "won't echo :("
In tenetur velit consequatur sed dolores omnis et similique.
crash! // on stderr
```
