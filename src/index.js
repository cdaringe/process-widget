'use strict'

const cas = require('casual')

module.exports = {
  run: function () {
    debugger
    console.log(cas.sentence)
    setInterval(() => console.log(cas.sentence), 1000)
  },
  runGracefulExit: function () {
    console.log(cas.sentence)
    setTimeout(() => console.log(cas.sentence), 1000)
  },
  runCrash: function () {
    console.log(cas.sentence)
    setTimeout(() => {
      console.error('crash!')
      process.exit(1)
    }, 0.5 + Math.random())
  }
}
