'use strict'

const commander = require('commander')
const pkg = require('../package.json')
const app = require('./app')

commander.version(pkg.version)
.option('run', 'run the widget indefinitely  [default]')
.option('run-graceful-exit', 'run and exit gracefully after 1 second')
.option('run-crash', 'ran and exit with error code 1 between 0.5-1.5 seconds')
.parse(process.argv)

if (commander.run) app.mode = 'run'
else if (commander.runGracefulExit) app.mode = 'runGracefulExit'
else if (commander.runCrash) app.mode = 'runCrash'
else {
  if (app.mode !== 'run')  // permit default
    throw new Error('invalid mode provided. run/runGracefulExit/runCrash permitted')
}
